package MySQL::Apache;

use strict;
use warnings 'all';

use MySQL::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{mysql} = MySQL::Keeper->new($state->{mysql});
}

sub request_init {
}

sub child_exit {
}

1;
