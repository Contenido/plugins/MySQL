package MySQL::Init;

use strict;
use warnings 'all';
use vars qw($VERSION @ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw ( $mysql $mstate );

use Contenido::Globals;
use MySQL::Globals;
use MySQL::Apache;
use MySQL::Keeper;

# загрузка всех необходимых плагину классов
# MySQL::SQL::SomeTable
# MySQL::SomeClass
Contenido::Init::load_classes(qw(
	));

sub init {
    warn "Contenido Init: Инициализация MySQL ($$)\n"   if ($DEBUG);

    $mstate = MySQL::State->new();
    $mysql = MySQL::Keeper->new($mstate);

    0;
}

1;
