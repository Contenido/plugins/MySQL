package MySQL::Globals;

use strict;
use warnings 'all';
use vars qw($VERSION @ISA @EXPORT $mysql $mstate);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw ( $mysql $mstate );

$VERSION =	'1.0';

$mysql = undef;
$mstate = undef;

1;
